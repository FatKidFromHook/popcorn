﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance { get; private set; }
    public Transform player;
    public Transform followCamera;
    public Vector3 deathCamPosition;
    public bool jumpZoom;
    public bool staticFollow;
    public float xMod;
    private float yMod;
    public float smoothness = 0.2f;
    private float zMod;
    private Animator animator;

    private Vector3 currrentVelocity = Vector3.zero;

    private void Awake()
    {
        Instance = this;
        animator = followCamera.GetComponentInChildren<Animator>();
    }

    void Update()
    {
        if (jumpZoom && PlayerController.Instance.isBig)
        {
            zMod = -player.position.y - 18.5f;
            yMod = player.position.y + 6.0f;
        }
        else if (/*UIManager.Instance.GameOver*/ PlayerController.Instance.IsDead && !staticFollow)
        {
            zMod = deathCamPosition.z;
            yMod = deathCamPosition.y;
            xMod = deathCamPosition.x;
        }
        else
        {
            zMod = -15.0f;
            yMod = 9.0f;
            xMod = PlayerController.Instance.bernardRB.velocity.magnitude > 10.0f ? 10.0f : 10.0f;
        }
    }

    private void FixedUpdate()
    {
        //if (staticFollow)
        //{
        //    staticFollow = false;
        //    followCamera.position = new Vector3(player.position.x + xMod, followCamera.position.y, followCamera.position.z);
        //}
        //else
        //{
        float magMod = Mathf.Clamp(PlayerController.Instance.bernardRB.velocity.magnitude, 0.0f, 30.0f);
        followCamera.position = Vector3.SmoothDamp(followCamera.position, new Vector3(player.position.x + xMod + magMod, yMod, zMod), ref currrentVelocity, smoothness);
        //}
    }

    public void ResetCamera()
    {
        followCamera.position = new Vector3(player.position.x + xMod, followCamera.position.y, followCamera.position.z);
    }

    public void SettingsIn()
    {
        if (animator != null)
            animator.Play("MainCameraSettingsIn");
    }

    public void SettingsOut()
    {
        if (animator != null)
            animator.Play("MainCameraSettingsOut");
    }
}
