﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionFragment : MonoBehaviour
{
    private Vector3 startingPos;
    private Rigidbody fragmentRB;
    private float spawnTime;
    float explosionForce;
    bool hasExploded = false;
    public bool isCharged = false;

    private void Awake()
    {
        startingPos = this.transform.localPosition;
        fragmentRB = this.GetComponent<Rigidbody>();
        fragmentRB.mass = 2;
    }

    private void OnDisable()
    {
        isCharged = true;
    }
    private void OnEnable()
    {
        fragmentRB.velocity = Vector3.zero;
        this.transform.localPosition = startingPos;
        this.transform.rotation = Quaternion.identity;
        hasExploded = false;
        explosionForce = 25.0f;
        spawnTime = Time.time;
        fragmentRB.isKinematic = false;
        //explosionForce = PlayerController.Instance.isBig ? 24.0f : 20.0f;
        //float explosionForce = PlayerController.Instance.isBig ? 12.0f : 10.0f;
        //Debug.LogWarning("Name: " + this.name + " Position: " + this.transform.parent.position);
        //fragmentRB.AddExplosionForce(explosionForce, this.transform.parent.position, 1.0f, 50.0f, ForceMode.Impulse);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (float.IsNaN(this.transform.position.x) || float.IsNaN(this.transform.position.y) || float.IsNaN(this.transform.position.z) || this.transform.position.y <= -50.0f)
            gameObject.SetActive(false);

        if (Time.time > spawnTime + 0.01f)
        {
            if (this.transform.parent.position != Vector3.zero)
            {
                if (!hasExploded && isCharged)
                {
                    hasExploded = true;
                    fragmentRB.AddExplosionForce(explosionForce, this.transform.parent.position, 1.0f, 50.0f, ForceMode.Impulse);
                }
            }
        }

      
        //else if (Time.time > spawnTime + 3.0f)
        //{
        //    this.transform.position = Vector3.zero;
        //}


        //I think this might cause the NaN errors.
        //Debug.LogWarning(fragmentRB.velocity);
        //Vector3 vel = fragmentRB.velocity;
        //vel.y -= 10 * Time.deltaTime;
        //fragmentRB.velocity = vel;

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            if(SettingsView.Instance.HapticOn && VibrationManager.HasVibrator())
                VibrationManager.Vibrate(5);
        }

    }
}
