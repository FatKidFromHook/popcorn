﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragmentHelper : MonoBehaviour
{
    private List<GameObject> fragments = new List<GameObject>();

    private void Awake()
    {
        FindFragments();
    }

    private void OnEnable()
    {

            foreach (GameObject go in fragments)
            {
                go.SetActive(true);
            }
    }

    private void OnDisable()
    {

            //foreach (GameObject go in fragments)
            //{
            //    go.SetActive(false);
            //}
    }

    public void FindFragments(bool isClone = false)
    {
        fragments.Clear();

        fragments.AddRange(GameObject.FindGameObjectsWithTag("Fragment"));

        if (isClone)
            this.gameObject.SetActive(false);
    }
}
