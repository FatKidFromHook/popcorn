﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDHelper : MonoBehaviour
{
    private void PauseMenuCloseNotify()
    {
        if (UIManager.Instance.GameOver)
        {
            UIManager.Instance.hudAnimator.Play("PauseMenuFadeOut");
        }
    }

    private void PauseMenuFadeOutNotify()
    {
        if (UIManager.Instance.GameOver)
        {
            UIManager.Instance.startMenuAnimator.Play("StartMenuFadeIn");
        }
    }

    private void PauseMenuStartUpNotify()
    {
        if (UIManager.Instance.GameOver)
        {
            UIManager.Instance.startMenuAnimator.Play("StartMenuFadeIn");
        }
    }

    private void HUDGameOverNotify()
    {
        PlayerController.Instance.bernardCanvas.SetActive(true);
    }

    private void HUDBackToMainMenuNotify()
    {
        if (UIManager.Instance.GameOver)
        {
            UIManager.Instance.startMenuAnimator.speed = 2.0f;
            UIManager.Instance.startMenuAnimator.Play("StartMenuFadeIn");
        }
    }

    private void HUDPauseButtonClosedNotify()
    {
        UIManager.Instance.pauseButtonText.text = "Pause";
    }

    private void HUDPauseButtonOpenNotify()
    {
         UIManager.Instance.pauseButtonText.text = "Resume";
    }
}
