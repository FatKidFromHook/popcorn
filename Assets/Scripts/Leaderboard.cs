﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboard : MonoBehaviour
{
    public struct ScoreEntry
    {
        public int Rank;
        public string Name;
        public int Score;
        public int HighMultiplier;

        public ScoreEntry(int rank, string name, int score, int highMultiplier)
        {
            Rank = rank;
            Name = name;
            Score = score;
            HighMultiplier = highMultiplier;
        }

        public void AddName(string newName)
        {
            Name = newName;

            Leaderboard.Instance.UpdateLeaderboard();
        }

    }
    public static Leaderboard Instance { get; private set; }
    public GameObject containerLeaderboard;
    private List<ScoreEntry> LeaderboardEntries = new List<ScoreEntry>();
    private List<Text> LeaderboardEntryNameText = new List<Text>();
    private List<Text> LeaderboardEntryHighScoreText = new List<Text>();
    //private List<Text> LeaderboardEntryHighestMultiplierText = new List<Text>();
    public ScoreEntry newHighScoreEntry;
    // Use this for initialization
    private void Awake()
    {
        Instance = this;
        InitLeaderboardText();
        InitLeaderboard();
    }

    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public int HighScore
    {
        get { return LeaderboardEntries[0].Score; }
    }

    public bool CheckForHighScore(int newScore)
    {
        if (newScore > LeaderboardEntries[LeaderboardEntries.Count - 1].Score)
        {
            return true;
        }
        return false;
    }

    private void InitLeaderboard()
    {
        if (ES2.Exists("saveData.txt"))
        {
            LoadLeaderboard();
        }
        else
        {
            GenerateDefaultLeaderboard();
            SaveLeaderboard();
        }
        UpdateLeaderboard();
    }

    public void UpdateLeaderboard()
    {
        for(int i = 0; i <  LeaderboardEntries.Count; i++)
        {
            LeaderboardEntryNameText[i].text = LeaderboardEntries[i].Name;
            LeaderboardEntryHighScoreText[i].text = String.Format("{0:n0}", LeaderboardEntries[i].Score);
            //LeaderboardEntryHighestMultiplierText[i].text = "x" + LeaderboardEntries[i].HighMultiplier.ToString();
        }
    }

    public void CreateNewLeaderboardEntry(string name, int score)
    {
        if(string.IsNullOrEmpty(name))
        {
            name = "AAA";
        }

        LeaderboardEntries.RemoveAt(LeaderboardEntries.Count - 1);
        ScoreEntry newEntry = new ScoreEntry(0, name, score, 0);
        newHighScoreEntry = newEntry;
        LeaderboardEntries.Add(newEntry);
        SortLeaderboard();
        UpdateLeaderboard();
        SaveLeaderboard();
    }

    public void ResetLeaderboard()
    {
        LeaderboardEntries.Clear();

        GenerateDefaultLeaderboard();
        SaveLeaderboard();
        UpdateLeaderboard();
        UIManager.Instance.UpdateHighScore();
    }

    private void SortLeaderboard()
    {
        LeaderboardEntries = LeaderboardEntries.OrderBy(x => -x.Score).ToList();
    }

    private void InitLeaderboardText()
    {
        Transform leaderboardEntryRoot = containerLeaderboard.transform.GetChild(0).GetChild(0);

        for (int i = 9; i >= 0; i--)
        {
            Text nameText = leaderboardEntryRoot.GetChild(i).GetChild(2).GetComponentInChildren<Text>();
            Text highScoreText = leaderboardEntryRoot.GetChild(i).GetChild(3).GetComponentInChildren<Text>();
            //Text highestMultiplierText = leaderboardEntryRoot.GetChild(i).GetChild(3).GetComponentInChildren<Text>();

            LeaderboardEntryNameText.Add(nameText);
            LeaderboardEntryHighScoreText.Add(highScoreText);
            //LeaderboardEntryHighestMultiplierText.Add(highestMultiplierText);
        }
    }

    private void SaveLeaderboard()
    {
        for(int i = 0; i < LeaderboardEntries.Count; i++)
        {
            ES2.Save(LeaderboardEntries[i].Name, "saveData.txt?tag=LeaderboardName" + i);
            ES2.Save(LeaderboardEntries[i].Score, "saveData.txt?tag=LeaderboardScore" + i);
        }
    }

    private void LoadLeaderboard()
    {
        LeaderboardEntries.Clear();
        for (int i = 0; i < 10; i++)
        {
            string name = ES2.Load<string>("saveData.txt?tag=LeaderboardName" + i);
            int score = ES2.Load<int>("saveData.txt?tag=LeaderboardScore" + i);
            ScoreEntry newEntry = new ScoreEntry(0, name, score, 0);
            LeaderboardEntries.Add(newEntry);
        }
    
    }

    private void GenerateDefaultLeaderboard()
    {
        for (int i = 0; i < 10; i++)
        {
            ScoreEntry newEntry = new ScoreEntry(i, "AAA", 50, 0); //Leave at one for easy testing. Set to 1000 for prod.
            LeaderboardEntries.Add(newEntry);
        }
    }
}
