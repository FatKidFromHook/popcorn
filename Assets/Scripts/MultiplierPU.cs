﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplierPU : MonoBehaviour
{
    public int multiplierValue;
    public Vector3 rotateDirection;
    public float rotateSpeed;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if (!UIManager.Instance.GameOver && !UIManager.Instance.GamePaused)
        {
            if (rotateDirection != Vector3.zero)
                this.transform.Rotate(rotateDirection * (Time.deltaTime * rotateSpeed));

        }
    }

    public void ShowMultiplierPU()
    {
        if(!this.GetComponent<MeshRenderer>().enabled)
            this.GetComponent<MeshRenderer>().enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bernard" && !UIManager.Instance.GameOver)
        {
            this.GetComponent<MeshRenderer>().enabled = false;
            this.GetComponent<ParticleSystem>().Play();
            
            UIManager.Instance.Multiplier += multiplierValue;
        }
    }
}
