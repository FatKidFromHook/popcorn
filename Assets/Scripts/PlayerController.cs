﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance { get; private set; }
    public GameObject bernard;
    public GameObject bernardCanvas;
    public GameObject bernardCube;
    public GameObject bernardSphere;
    public GameObject bernardFragments;
    public GameObject containerFragmentsPool;
    private GameObject currentFragment;
    public Rigidbody bernardRB;
    public Vector3 bernardStartPos;
    private List<GameObject> fragmentPool = new List<GameObject>();

    public bool isBig;
    bool isGrounded = true;
    bool isFalling;
    bool canDoubleJump;
    bool canMorphColor;
    public Color32 smallColor;
    public Color32 bigColor;

    public float jumpForce1;
    public float jumpForce2;
    public float moveSpeed;
    public float maxSpeed;
    public float boostSpeed;
    public float gravityMultiplier;
    public float bigGravityMultiplier;
    public float jumpCeiling;
    public float bigJumpCeiling;
    public float dashForce;
    public float dashDuration;
    public float dashDradMod;
    public float tick;
    public Animator animator;
    public WorldGenerator worldGenerator;

    public bool isDebug = false;
    public float heightDebug;
    public float velDebug;
    public float colorMorphSpeed;
    private bool canMove;
    private bool isDead = false;
    private bool isDashing;
    private float dashStartTime = -1;
    private int newFragmentAmount = 100;

    public object CrossPlatformInput { get; private set; }
    public bool CanMove { get { return canMove; } set { canMove = value; } }

    private void Awake()
    {
        Instance = this;
        //bernard = GameObject.FindGameObjectWithTag("Bernard");
        bernardRB = bernard.GetComponent<Rigidbody>();
        bernardCube.GetComponent<MeshRenderer>().material.color = Color.blue;
        bernardStartPos = this.transform.position;

    }
    // Use this for initialization
    void Start ()
    {
        InitFragments();

    }
	
	// Update is called once per frame
	void Update ()
    {

        //if (!UIManager.Instance.GameOver && canMove)
        //{
        //    bernardRB.AddForce(Vector3.right * moveSpeed, ForceMode.Acceleration);
        //}

        if (Input.GetKeyDown(KeyCode.Space) || CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            if (canDoubleJump)
            {
                Jump();
            }
        }

        if (isDashing)
            Dash();
        else if(!isDead)
            bernardRB.useGravity = true;

        //if (((Input.GetKeyDown(KeyCode.A) || CrossPlatformInputManager.GetButtonDown("Morph"))))
        //{
        //    MorphPlayer();
        //    canMorphColor = true;
        //}

        if (canMorphColor && !UIManager.Instance.GameOver)
            MorphColor();

        FallAssist();
        CapSpeed();

        if (isDebug)
        {
            DebugValues();
        }
    }

    private void FixedUpdate()
    {
        //if (!UIManager.Instance.GameOver && canMove)
        //{
        //    bernardRB.AddForce(Vector3.right * moveSpeed, ForceMode.Acceleration);
        //}
    }

    private void LateUpdate()
    {
        if (!UIManager.Instance.GameOver && canMove)
        {
            if (bernardCube.transform.localPosition != Vector3.zero)
                bernardCube.transform.localPosition = Vector3.zero;
            bernardRB.AddForce(Vector3.right * moveSpeed, ForceMode.Acceleration);
        }

    }

    public bool IsDead { get { return isDead; } }

    public void Jump()
    {
        heightDebug = 0;
        if (UIManager.Instance.GameOver)
            return;

        if(SettingsView.Instance.HapticOn && VibrationManager.HasVibrator())
            VibrationManager.Vibrate(10);

        if (!isGrounded && canDoubleJump)
        {
            //isFalling = false;
            canDoubleJump = false;
        }
        isGrounded = false;
        animator.SetBool("IsGrounded", false);
        ForceMode fm = isFalling ? ForceMode.Impulse : ForceMode.Impulse;
        //Vector3 dir = isFalling ? (Vector3.up + (Vector3.forward*5)) : Vector3.up;
        //Vector3 dir = canDoubleJump ? Vector3.up : Vector3.right;
        float bigJumpForce = isFalling ? jumpForce2 * 2.0f : jumpForce2;
        float smallJumpForce = isFalling ? jumpForce1 * 2.75f : jumpForce1;

        if (isBig)
        {
            //maxSpeed = 30.0f;
            if (canDoubleJump)
                bernardRB.AddForce(Vector3.up * bigJumpForce, fm);
            else
                isDashing = true;

            animator.Play("BigJump");
            //Debug.LogWarning("Big Jump - Is Falling: " + isFalling + " Jump Force: " + bigJumpForce);
        }
        else
        {
            //maxSpeed = 21.0f;
            bernardRB.AddForce(Vector3.up * smallJumpForce, fm);
            animator.Play("Jump");
            //Debug.LogWarning("Small Jump - Is Falling: " + isFalling + " Jump Force: " + smallJumpForce);
        }
    }

    private void Dash()
    {
        bernardRB.useGravity = false;
        bernardRB.drag = dashDradMod;

        //Vector3 newPos = new Vector3(bernardRB.transform.localPosition.x + 15.0f, bernardRB.transform.localPosition.y, bernardRB.transform.localPosition.z);

        //bernardRB.MovePosition(Vector3.Lerp(bernardRB.transform.localPosition, newPos, dashDuration * Time.deltaTime));



        if (dashStartTime < 0)
        {
            if(isFalling)
            {
                Vector3 newVel = new Vector3(bernardRB.velocity.x, 0, bernardRB.velocity.z);
                bernardRB.velocity = newVel;
            }
                
            bernardRB.AddForce(Vector3.right * dashForce, ForceMode.VelocityChange);
            dashStartTime = Time.time;
        }

        if (Time.time > dashStartTime + dashDuration || isGrounded)
        {
            isDashing = false;
            dashStartTime = -1;
            bernardRB.useGravity = true;
            bernardRB.drag = 0;
        }
    }

    private void FallAssist()
    {
        float maxheight = (isBig ? bigJumpCeiling : jumpCeiling);

        //These values seem to work but if the other values start to change it could effect this.
        //TODO make this global & public.
        if (!canDoubleJump)
            maxheight += (isBig ? 4.0f : 2.0f);

        if (!isGrounded && bernardRB.velocity.y < 0)
            isFalling = true;

        if ((!isGrounded && bernardRB.transform.position.y >= maxheight) || isFalling)
        { 
            Vector3 vel = bernardRB.velocity;
            vel.y -= (isBig ? bigGravityMultiplier : gravityMultiplier) * Time.deltaTime;
            bernardRB.velocity = vel;
        }
    }

    public void MorphPlayer(bool forceSmall = false)
    {
        if(!UIManager.Instance.GameOver || forceSmall)
        { 
            isBig = forceSmall ? false : !isBig;

            if (isBig)
            {
                //animator.Play("BernardMorphBig");

                bernard.transform.localScale = new Vector3(1.0f, 2.0f, 0.5f);
                if (isGrounded)
                    bernard.transform.position = new Vector3(bernard.transform.position.x, 1.51f, 0.0f);
            }
            else
            {
                //animator.Play("BernardMorphSmall");

                bernard.transform.localScale = new Vector3(1.0f, 1.0f, 0.5f);

                if (isGrounded)
                    bernard.transform.position = new Vector3(bernard.transform.position.x, 1.01f, 0.0f);
            }
            tick = 0;
        }
    }

    private void MorphColor(bool forceSmallColor = false)
    {
        if (forceSmallColor)
        {
            bernardCube.GetComponent<MeshRenderer>().material.color = smallColor;
            return;
        }

        if (tick< 1)
        { // while t below the end limit...
          // increment it at the desired rate every update:
            tick += Time.deltaTime / colorMorphSpeed;
        }
        else
        {
            canMorphColor = false;
        }

        if (isBig)
            bernardCube.GetComponent<MeshRenderer>().material.color = Color.Lerp(smallColor, bigColor, tick);
        else
            bernardCube.GetComponent<MeshRenderer>().material.color = Color.Lerp(bigColor, smallColor, tick);
    }

    private void CapSpeed()
    {

        if (bernardRB.velocity.magnitude > maxSpeed)
        {
            if(bernardRB.velocity.magnitude > 50.0f /*maxSpeed * 2.5f*/)
                bernardRB.drag = 3;

            bernardRB.velocity = Vector3.Lerp(bernardRB.velocity, bernardRB.velocity.normalized * maxSpeed, 1.1f * Time.deltaTime);
        }
        else
        {
            if(!isDashing)
                bernardRB.drag = 0;
        }
    }

    private void Dead()
    {
        this.gameObject.GetComponent<BoxCollider>().enabled = false;
        bernardRB.useGravity = false;
        bernardRB.isKinematic = true;
        canMove = false;
        SpawnFragments();
        bernardSphere.SetActive(false);
        UIManager.Instance.GameOver = true;
        UIManager.Instance.Restart = true; //Should this be here?
        if(!UIManager.Instance.CheckHighScore())
        {
            UIManager.Instance.hudAnimator.Play("HUDGameOver");
        }
    }

    private void SpawnFragments()
    {
        currentFragment = fragmentPool[0];
        fragmentPool.Remove(currentFragment.gameObject);

        currentFragment.transform.localPosition = bernardCube.transform.position;
        //currentFragment.GetComponent<ExplosionFragment>().isCharged = true;

        bernardCube.SetActive(false);
        currentFragment.gameObject.SetActive(true);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "KillCollider")
        {
            if(!isDead)
            {
                if(SettingsView.Instance.HapticOn && VibrationManager.HasVibrator())
                    VibrationManager.Vibrate(15);

                //Debug.Break();
                isDead = true;
                animator.SetBool("IsDead", true);
                Dead();
                return;
            }
        }

        if (!isGrounded)
        {
            //Debug.Log("Boost");
            bernardRB.AddForce(Vector3.right * boostSpeed, ForceMode.Impulse);
        }
        bernardSphere.transform.localScale = Vector3.one / 2;
        isFalling = false;
        isGrounded = true;
        animator.SetBool("IsGrounded", true);
        canDoubleJump = true;
        //maxSpeed = 20.0f;

        if (bernardCube.activeSelf)
            animator.Play("idle");

        if (isDebug)
        {
            //Debug.LogWarning("Max height: " + heightDebug);
            //Debug.LogWarning("Max vel: " + velDebug);
        }

        if (collision.gameObject.tag == "Trap")
        {
            if(SettingsView.Instance.HapticOn && VibrationManager.HasVibrator())
                VibrationManager.Vibrate(50);

            if (!UIManager.Instance.GameOver)
            {
                TrapController tc = collision.gameObject.GetComponentInChildren<TrapController>();
                if (tc != null)
                {
                    //temp = collision.transform.parent.GetComponentInChildren<TrapController>();
                    tc.WasHit();
                    
                    if (tc.isPenalty)
                        UIManager.Instance.Multiplier = 1;
                }
                isFalling = true;
            }
        }
    }

    public void ResetBernard()
    {
        //CameraController.Instance.staticFollow = true;
        isDead = false;
        animator.SetBool("IsDead", false);
        bernardCanvas.transform.GetChild(0).gameObject.SetActive(false);
        bernardCanvas.SetActive(false);

        this.gameObject.GetComponent<BoxCollider>().enabled = true;
        bernardRB.useGravity = true;
        bernardRB.isKinematic = false;
        //MorphPlayer(true);
        //MorphColor(true);
        bernardRB.velocity = Vector3.zero;
        bernard.transform.position = bernardStartPos;
        bernardCube.transform.rotation = Quaternion.identity;
        CameraController.Instance.ResetCamera();
        if(currentFragment != null)
            Destroy(currentFragment.gameObject);
        //bernardFragments.SetActive(false);
        bernardCube.transform.localPosition = Vector3.zero;
        bernardCube.SetActive(true);
        worldGenerator.ResetLevel();
    }

    private void InitFragments()
    {
        if (bernardFragments != null)
        {
            bernardFragments.transform.parent = containerFragmentsPool.transform;
            bernardFragments.transform.localPosition = new Vector3(-100, 0, 0);

            fragmentPool.Add(bernardFragments);
            bernardFragments.SetActive(false);

            for (int i = 0; i < newFragmentAmount; i++)
            {
                GameObject newFragment = Instantiate(bernardFragments, Vector3.zero, Quaternion.identity);
                newFragment.transform.parent = containerFragmentsPool.transform;

                newFragment.transform.localPosition = new Vector3(-100, 0, 0);
                newFragment.SetActive(true);
                newFragment.GetComponent<FragmentHelper>().FindFragments(true);
                fragmentPool.Add(newFragment);

            }
        }
    }

    private void DebugValues()
    {
        if (bernardRB.transform.position.y > heightDebug)
        {
            heightDebug = bernardRB.transform.position.y;
        }

        //if (bernardRB.velocity.magnitude > velDebug)
        //{
        velDebug = bernardRB.velocity.magnitude;
        //}
    }
}
