﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsView : MonoBehaviour
{
    public static SettingsView Instance { get; private set; }
    public Text hapticButtonText;
    public Animator settingsAnimator;

    private bool hapticOn = true;

    private void Awake()
    {
        Instance = this;
        InitSettings();
    }
    // Use this for initialization
    void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public bool HapticOn { get { return hapticOn; } }
    
    public void OnResetLeaderboardClick()
    {
        Leaderboard.Instance.ResetLeaderboard();
    }

    public void OnToggleHapticClick()
    {
        hapticOn = !hapticOn;
        UpdateHapticButtonText();
    }

    public void OnCloseSettingsViewClick()
    {
        CloseSettingsView();
    }

    public void OpenSettingsView()
    {
        settingsAnimator.Play("SettingsFadeIn");
    }

    private void CloseSettingsView()
    {
        SaveSettings();
        settingsAnimator.Play("SettingsFadeOut");
        UIManager.Instance.hudAnimator.Play("HUDFadeFromSettings");
        UIManager.Instance.startMenuAnimator.Play("StartMenuFadeFromSettings");
        CameraController.Instance.SettingsOut();
    }

    private void UpdateHapticButtonText()
    {
        if (hapticOn)
        {
            hapticButtonText.text = "Haptic: On";
        }
        else
        {
            hapticButtonText.text = "Haptic: Off";
        }
    }

    private void InitSettings()
    {
        if (ES2.Exists("saveData.txt"))
        {
            LoadSettings();
            UpdateHapticButtonText();
        }
        else
        {
            hapticOn = true;
            UpdateHapticButtonText();
        }
    }

    private void SaveSettings()
    {
        ES2.Save(hapticOn, "saveData.txt?tag=HapticOn");
    }

    private void LoadSettings()
    {
        if(ES2.Exists("saveData.txt?tag=HapticOn"))
            hapticOn = ES2.Load<bool>("saveData.txt?tag=HapticOn");
    }
}
