﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuHelper : MonoBehaviour
{
    public static StartMenuHelper Instance { get; private set; }
    public Transform ground;
    public Transform player;

    public Vector3 groundStartPos;
    public Vector3 groundOffsetPos;
    public Vector3 playerStartPos;
    public Vector3 playerOffsetPos;
    public float groundMoveSpeed;
    public float playerMoveSpeed;
    public bool canAnimateGround;
    public bool playerAnimComplete;
    private bool canAnimatePlayer;
    private bool animateFromLeft;

    private void Awake()
    {
        Instance = this;
        groundStartPos = ground.position;
        playerStartPos = player.position;
    }

    void Start ()
    {
        ground.position = groundOffsetPos;
        player.position = playerOffsetPos;
        player.gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!UIManager.Instance.Restart)
        {
            if (canAnimateGround)
            {
                AnimateGround();
            }

            if (canAnimatePlayer)
            {
                if (!player.gameObject.activeSelf)
                    player.gameObject.SetActive(true);
                AnimatePlayer();
                //UIManager.Instance.hudAnimator.Play("PauseMenuStartUp");

            }
        }

    }

    private void AnimateGround()
    {
        float t = Time.fixedDeltaTime * (animateFromLeft ? groundMoveSpeed/2 : groundMoveSpeed);
        ground.position =  Vector3.Lerp(ground.position, groundStartPos, t);
        if ((animateFromLeft && ground.position.x >= groundStartPos.x - 0.01f) || (!animateFromLeft && ground.position.x <= groundStartPos.x + 0.01f)) //Using staticFollow here because it if a good flag for this...this is ugly.
        {
            canAnimateGround = false;
            canAnimatePlayer = true;
        }
    }

    private void AnimatePlayer()
    {
        float t = Time.fixedDeltaTime * (animateFromLeft ? playerMoveSpeed / 2 : playerMoveSpeed);
        player.position = Vector3.Lerp(player.position, playerStartPos, t);
        if(player.position.y >= playerStartPos.y - 0.01f)
        {
            canAnimatePlayer = false;
        }
        else if(animateFromLeft && UIManager.Instance.Restart)
        {
            UIManager.Instance.hudAnimator.Play("HUDGameOverRestart");
        }
        else if (UIManager.Instance.FirstPass && player.position.y >= playerStartPos.y - 0.2f)
        {
            UIManager.Instance.hudAnimator.Play("PauseMenuStartUp");
        }
    }

    private void StartMenuTitleFadeOutNotify()
    {
        canAnimateGround = true;
    }

    private void StartMenuFadeInNotify()
    {
        if (UIManager.Instance.Restart)
        {
            //UIManager.Instance.Restart = false;
            UIManager.Instance.StartGame();
        }
    }

    public void ResetStart()
    {
        ground.position = new Vector3(groundStartPos.x - 10.0f, groundStartPos.y, groundStartPos.z);
        player.position = playerOffsetPos;
        player.gameObject.SetActive(false);
        animateFromLeft = true;
        canAnimatePlayer = true;
        canAnimateGround = true;

    }
}
