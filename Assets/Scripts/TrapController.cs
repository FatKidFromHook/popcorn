﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapController : MonoBehaviour
{
    public GameObject upper;
    public MultiplierPU multiplierPU;

    public Vector3 startPosition;
    public Vector3 scale;
    public int score;
    public bool wasHit;
    public bool isBigtrap;
    public bool isPenalty;
    public BoxCollider deathCollider;
    private Color trapColor;
    private Color pulseColor;
    private MeshRenderer trapMR;
    private MeshRenderer upperMR;
    public bool canPulseColor;
    public bool pinPong;
    public Vector3 rotateDirection;
    public Animator animator;
    [Tooltip("This value represents half of the total travel distance.")]
    public float pinPongDistance;
    public float pinPongSpeed;
    public float bufferDistance;
    public float rotateSpeed;
    public MeshRenderer penaltyQuadMR;
    public Material[] penaltyQuadMats;

    //Trap type default settings
    //Vector3 smallCubeScale = new Vector3(1.0f, 1.0f, 0.5f);
    //Vector3 smallPos1 = new Vector3(0.0f, 0.0f, 0.5f);
    //Vector3 smallPos2 = new Vector3(0.0f, 0.5f, 0.5f);
    //Vector3 smallPos3 = new Vector3(0.0f, 1.1f, 0.5f);

    private void Awake()
    {
        trapMR = this.GetComponent<MeshRenderer>();
        //trapColor = trapMR.material.color;
        animator = this.GetComponent<Animator>();
        if (upper != null)
        {
            upperMR = upper.GetComponent<MeshRenderer>();
        }
    }

    private void OnEnable()
    {
        SetColors();

        if (penaltyQuadMR != null)
            penaltyQuadMR.material = penaltyQuadMats[1];
    }

    public void ResetMultiplierPU()
    {
        if(multiplierPU != null)
            multiplierPU.ShowMultiplierPU();

        if(penaltyQuadMR != null)
            penaltyQuadMR.material = penaltyQuadMats[1];

    }

    public void WasHit()
    {
        if (penaltyQuadMR != null)
        {
            wasHit = true;
            penaltyQuadMR.material = penaltyQuadMats[0];
        }
    }

    private void SetColors()
    {
        float trapValue;
        float pulseValue;

        if (isBigtrap)
        {
            trapValue = UnityEngine.Random.Range(140, 255);
            if (trapValue >= 200)
                pulseValue = UnityEngine.Random.Range(0, 255);
            else
                pulseValue = UnityEngine.Random.Range(0, 120);
        }
        else
        {
            trapValue = UnityEngine.Random.Range(140, 255);
            if (trapValue >= 220)
                pulseValue = UnityEngine.Random.Range(0, 255);
            else
                pulseValue = UnityEngine.Random.Range(0, 120);
        }

        if (isBigtrap)
        {
            trapColor = new Color32((byte)trapValue, 0, (byte)trapValue, 255);
            pulseColor = new Color32((byte)trapValue, (byte)pulseValue, (byte)trapValue, 255);
        }
        else
        {
            trapColor = new Color32(0, 0, (byte)trapValue, 255);
            pulseColor = new Color32(0, (byte)pulseValue, (byte)trapValue, 255);
        }
    }

    private void Update()
    {
        if(canPulseColor)
            ColorPulse();

        if (!UIManager.Instance.GameOver && !UIManager.Instance.GamePaused)
        {
            if (rotateDirection != Vector3.zero)
                this.transform.Rotate(rotateDirection * (Time.deltaTime * rotateSpeed));

            if (pinPong)
            {
                PingPong();
            }
        }
    }

    private void ColorPulse()
    {
        trapMR.material.color = Color.Lerp(trapColor, pulseColor, Mathf.PingPong(Time.time, 0.5f));

        if(upper != null)
            upperMR.material.color = Color.Lerp(trapColor, pulseColor, Mathf.PingPong(Time.time, 0.5f));

    }

    private void PingPong()
    {
        float newX = transform.localPosition.x - 0.1f;
        transform.localPosition = new Vector3(newX, (Mathf.Sin(Time.time * pinPongSpeed) * pinPongDistance) + startPosition.y,  transform.localPosition.z);
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Bernard" && !UIManager.Instance.GameOver)
        {
            if (wasHit)
            {
                //animator.Play("TrapFlash");
                wasHit = false;
                return;
            }
    
            UIManager.Instance.AddScore(score);

        }
    }

}
