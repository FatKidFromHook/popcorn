﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance { get; private set; }
    public PlayerController bernard;
    public GameObject containerStartMenu;
    public GameObject containerLeaderboard;
    public GameObject containerNewHighScore;
    public Animator hudAnimator;
    public Animator startMenuAnimator;
    public Text pauseButtonText;
    public Text scoreText;
    public Text highScoreText;
    public Text multiplierText;
    public InputField newHighScoreIF;
    public Toggle[] toggles;
    private bool gameOver;
    private bool gamePaused;
    private bool restart;
    private bool firstPass = true;
    private int score;
    private int multiplier;
    private int previousScore;
    private bool leaderboardOpen;

    public bool GameOver { get { return gameOver; } set { gameOver = value; } }
    public bool GamePaused { get { return gamePaused; } }
    public bool Restart { get { return restart; } set { restart = value; } }
    public bool FirstPass { get { return firstPass; } set { firstPass = value; } }
    public int Score
    {
        get { return score; }
        set
        {
            score = value;
            UpdateScore();
        }
    }
    public int Multiplier
    {
        get { return multiplier; }
        set
        {
            multiplier = value;
            UpdateMultiplier();
        }
    }

    public int Difficulty
    {
        get
        {
            //if(toggles[0].isOn)
            //    return 0;
            //if (toggles[1].isOn)
            //    return 1;
            //if (toggles[2].isOn)
            //    return 2;

            return 1;
        }
    }


    private void Awake()
    {
        Instance = this;
        gameOver = true;
        gamePaused = false;
    }

    void Start ()
    {
        Time.timeScale = 0;
        Multiplier = 1;
        UpdateScore();
        UpdateHighScore();

        startMenuAnimator.Play("StartMenuTitleFadeOut");
    }

    void Update ()
    {
        //Move to it's own class.
        NewHighScoreNameInputHelper();

        if(Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            OnConfirmInitialsClick();
        }

    }

    public void ShowButtons(bool open)
    {
        if (open)
        {
            startMenuAnimator.Play("StartMenuFadeIn");
        }
        else
        {
            startMenuAnimator.Play("StartMenuFadeOut");
        }
    }

    public bool CheckHighScore()
    {
        StopCoroutine("ScoreTicker");
        UpdateScore(false);
        //Debug.Log("poop");

        if (Leaderboard.Instance.CheckForHighScore(score))
        {
            hudAnimator.Play("HUDNewHighScoreIn");
            return true;
        }
        return false;
    }

    public void AddScore(int trapValue)
    {
        int adjustedScore = trapValue * multiplier;
        previousScore = score;
        score += adjustedScore;
        UpdateScore();
    }

    public void UpdateMultiplier()
    {
        multiplierText.text = "x" + multiplier.ToString();

        if(multiplier > 1.0f)
            hudAnimator.Play("HUDAddMultiplier");
    }

    private void UpdateScore(bool tickerOn = true)
    {
       if(Score > 0 && tickerOn)
       {
           StartCoroutine("ScoreTicker");
       }
       else
       {
           scoreText.text = "Score: " + String.Format("{0:n0}",  score);
       }
    }

    private IEnumerator ScoreTicker()
    {
        int displayScore = previousScore;

        float tickInterval = 0.3f / (float)(Score - displayScore);

        while (displayScore < Score)
        {
            displayScore++;
            scoreText.text = "Score: " + String.Format("{0:n0}", displayScore);
            yield return new WaitForSeconds(tickInterval);
        }
    }

    public void UpdateHighScore()
    {
        if(Leaderboard.Instance != null)
            highScoreText.text = "High Score: " + String.Format("{0:n0}", Leaderboard.Instance.HighScore);
    }

    public void RestartGame()
    {
        gamePaused = false;
        bernard.ResetBernard();
        if (gameOver)
        {
            hudAnimator.Play("HUDGameOverRestart");
            startMenuAnimator.Play("StartMenuFadeIn");

            if (containerNewHighScore.activeSelf)
            {
                newHighScoreIF.text = string.Empty;
                hudAnimator.Play("HUDNewHighScoreFadeOut");
            }
        }
        else
        {
            gameOver = true;
            hudAnimator.Play("HUDPauseMenuRestart");
            startMenuAnimator.Play("StartMenuFadeIn");
        }
    }

    public void OnMainMenuClick()
    {
        gamePaused = false;
        restart = false;
        bernard.ResetBernard();

        if (gameOver)
        {
            hudAnimator.Play("HUDGameOverBackToHome");

            if(containerNewHighScore.activeSelf)
            {
                newHighScoreIF.text = string.Empty;
                hudAnimator.Play("HUDNewHighScoreFadeOut");
            }
        }
        else
        {
            gameOver = true;
            hudAnimator.Play("HUDPauseMenuBackToHome");
        }
    }

    public void StartGame()
    {
        this.GetComponent<WorldGenerator>().PlaceTraps();
        CameraController.Instance.staticFollow = false;
        Score = 0;
        UpdateScore();
        Multiplier = 1;
        gameOver = false;
        PlayerController.Instance.CanMove = true;
        Time.timeScale = 1;
        ShowButtons(false);

        if (restart)
        {
            restart = false;
        }
        else
        {
            hudAnimator.Play("PauseMenuFadeIn");
        }

        //This is closing the leaderbaord if it is open when the user starts the game. The method is poorly named for use in this manner.
        if(leaderboardOpen)
        {
            OpenLeaderboard();
        }

        if (pauseButtonText.text != "Pause")
            pauseButtonText.text = "Pause";

        if (FirstPass)
            FirstPass = false;
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void PauseGame()
    {
        if (gamePaused)
        {
            hudAnimator.Play("PauseMenuClose");
            Time.timeScale = 1;
            ShowButtons(false);
        }
        else
        {
            hudAnimator.Play("PauseMenuOpen");
            restart = true;
            Time.timeScale = 0;
        }
        gamePaused = !gamePaused;
    }

    public void OpenLeaderboard()
    {
        if (leaderboardOpen)
        {
            startMenuAnimator.Play("StartMenuLeaderboardCascadeOut");
            leaderboardOpen = false;
        }
        else
        {
            startMenuAnimator.Play("StartMenuLeaderboardCascadeIn");
            leaderboardOpen = true;
        }

    }

    public void OnOpenSettingsViewClick()
    {
        hudAnimator.Play("HUDFadeToSettings");
        startMenuAnimator.Play("StartMenuFadeToSettings");
        SettingsView.Instance.OpenSettingsView();
        CameraController.Instance.SettingsIn();

        //This is closing the leaderbaord if it is open when the user starts the game. The method is poorly named for use in this manner.
        if (leaderboardOpen)
        {
            leaderboardOpen = false;
            startMenuAnimator.Play("StartMenuLeaderboardQuickClose");
        }
    }

    public void OnConfirmInitialsClick()
    {
        Leaderboard.Instance.CreateNewLeaderboardEntry(newHighScoreIF.text, score);
        hudAnimator.Play("HUDNewHighScoreOut");
        UpdateHighScore();
        hudAnimator.Play("HUDNewHighScoreToGameOverMenu");
        PlayerController.Instance.bernardCanvas.SetActive(true);
    }

    private void NewHighScoreNameInputHelper()
    {
        string text = newHighScoreIF.text;
        if (text != newHighScoreIF.text.ToUpper())
        {
            newHighScoreIF.text = newHighScoreIF.text.ToUpper();
        }
    }

    public void ButtonVibrate()
    {
        if(SettingsView.Instance.HapticOn && VibrationManager.HasVibrator())
            VibrationManager.Vibrate(10);
    }

    public void SliderVibrate()
    {
        if (SettingsView.Instance.HapticOn && VibrationManager.HasVibrator())
            VibrationManager.Vibrate(1);
    }
}
