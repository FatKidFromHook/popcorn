﻿using UnityEngine;
using System.Collections;

public class UnscaledTimeParticle : MonoBehaviour
{
    ParticleSystem ps;
    private void Awake()
    {
        ps = this.gameObject.GetComponent<ParticleSystem>();
    }
    void Update()
    {
        if (Time.timeScale < 0.01f)
        {
            if(ps != null)
                ps.Simulate(Time.unscaledDeltaTime, true, false);
        }
    }
}