﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator : MonoBehaviour
{
    public GameObject trapPoolContainer;
    public GameObject groundPoolContainer;
    public GameObject starsPoolContainer;
    public GameObject ground;
    public GameObject stars;
    public GameObject[] traps;
    public Transform bernard;
    public Vector3 groundStartPos;
    public int newTrapAmount;

    private List<GameObject> trapPool = new List<GameObject>();
    private List<GameObject> currentTrapList = new List<GameObject>();
    private List<GameObject> groundPool = new List<GameObject>();
    private List<GameObject> starPool = new List<GameObject>();

    [SerializeField]
    private float[] difficultyInterval = new float[3];
    //private float trapInterval = 0;
    private float nextPosition = 50;

    private void Awake()
    {
        //trapInterval = difficultyInterval[UIManager.Instance.Difficulty];

        //Set up ground.
        InitGround();
        groundStartPos = groundPool[0].transform.position;

        InitStars();

        //Set up traps.
        InitTraps();

    }

    void Start ()
    {

    }
	
	void Update ()
    {

        //Move ground.
        for (int i = 0; i < groundPool.Count; i++)
        {
            if (bernard.position.x > groundPool[i].transform.position.x + (ground.transform.localScale.x/4))
            {
                groundPool[i].transform.position = new Vector3((groundPool[i].transform.position.x + (groundPool[i].transform.localScale.x / 4)), groundStartPos.y, groundStartPos.z);
                //stars.transform.localPosition = new Vector3(groundPool[i].transform.position.x, 0.0f, 0.0f);
            }
        }

        //Move stars.
        for (int i = 0; i < starPool.Count; i++)
        {
            if (bernard.position.x > starPool[i].transform.position.x + 50)
            {
                starPool[i].transform.position = new Vector3(starPool[i].transform.position.x + 300, 0.0f, 0.0f);
                //stars.transform.localPosition = new Vector3(groundPool[i].transform.position.x, 0.0f, 0.0f);
            }
        }

        //Move traps.
        if (currentTrapList.Count > 0 && !UIManager.Instance.GameOver && bernard.position.x > currentTrapList[0].transform.position.x + 100.0f)
        {
            ClearTrap();
        }

        if (trapPool.Count > 0 && !UIManager.Instance.GameOver && bernard.position.x > currentTrapList[currentTrapList.Count - 1].transform.position.x - 100.0f)
        {
            SpawnTrap();
      
        }
    }

    private void InitGround()
    {
        //GameObject newGround = Instantiate(ground, Vector3.zero, Quaternion.identity);
        //newGround.transform.parent = groundPoolContainer.transform;
        //newGround.transform.position = new Vector3(-ground.transform.localScale.x / 2, 0.0f, 0.0f);
        groundPool.Add(ground);
        //groundPool.Add(newGround);
    }

    private void InitStars()
    {
        for (int i = 1; i < 7; i++)
        {
            GameObject newStars = Instantiate(stars, Vector3.zero, Quaternion.identity);
            newStars.transform.parent = starsPoolContainer.transform;
            newStars.transform.position = new Vector3(50*i, 0.0f, 0.0f);
            //starPool.Add(stars);

            starPool.Add(newStars);
        }
        stars.SetActive(false);
    }

    public void PlaceTraps()
    {
        //Position all the traps
        //float nextPosition = 50;
        while (trapPool.Count > 0)
        {
            SpawnTrap();
        }
    }

    private void SpawnTrap()
    {
        if (trapPool.Count > 0)
        {


            int randomIndex = Random.Range(0, trapPool.Count - 1);
            GameObject tempGO = trapPool[randomIndex];
            if (!tempGO.activeSelf)
                tempGO.SetActive(true);
            TrapController tp = tempGO.gameObject.GetComponentInChildren<TrapController>();
            float newY = 100.0f;
            float oldY = tempGO.transform.position.y;
            tempGO.transform.position = new Vector3(nextPosition + tp.bufferDistance, newY, tempGO.transform.position.z);

            //newY = (CheckForGround(tempGO.transform) + tp.startPosition.y);
            newY = oldY;



            tempGO.transform.position = new Vector3(nextPosition + tp.bufferDistance, newY, tempGO.transform.position.z);
            tp.wasHit = false;
            tp.ResetMultiplierPU();
            //tempGO.transform.position = new Vector3(nextPosition + tp.bufferDistance, 0, 0);
            currentTrapList.Add(tempGO);
            trapPool.Remove(tempGO);
            if (tp.pinPong && trapPool.Count > 0)
                SpawnTrap();
            else
                nextPosition += (difficultyInterval[UIManager.Instance.Difficulty] + tp.bufferDistance);
        }
        else
        {
            Debug.LogError("Trap Pool is empty. Cannot spawn new trap.");
        }
    }

    private float CheckForGround(Transform t)
    {
        float groundHeight = -100;
        RaycastHit hit;
        if (Physics.Raycast(t.position, -Vector3.up, out hit))
        {
            Debug.Log(hit + " Tag: " + hit.collider.gameObject.tag);
            if (hit.collider.gameObject.tag == "Ground")
            {
                groundHeight = hit.point.y;
            }
            else
            {
                Vector3 temp = new Vector3(t.position.x + 0.5f, t.position.y, t.position.z);
                t.position = temp;
                return CheckForGround(t);
            }
               

        }

        Debug.Log(t + " Ground height: " + groundHeight);

       return groundHeight;
    }

    private void ClearTrap()
    {
        GameObject tempGO = currentTrapList[0];

        trapPool.Add(tempGO);
        currentTrapList.Remove(tempGO);
    }

    private void InitTraps()
    {
        //Load up trap pool.
        foreach (GameObject go in traps)
        {
            if (go.activeSelf != false)
            {
                go.transform.parent = trapPoolContainer.transform;
                //go.transform.localPosition = new Vector3(-100, go.transform.localPosition.y, go.transform.localPosition.z);
                go.transform.localPosition = new Vector3(-100, 0, 0);
                trapPool.Add(go);
                for (int i = 0; i < newTrapAmount; i++)
                {
                    GameObject newTrap = Instantiate(go, Vector3.zero, Quaternion.identity);
                    TrapController tp = newTrap.GetComponentInChildren<TrapController>();
                    newTrap.transform.parent = trapPoolContainer.transform;

                    if (tp != null)
                    {
                        //newTrap.transform.localPosition = new Vector3(-100, tp.startPosition.y, tp.startPosition.z);
                        newTrap.transform.localPosition = new Vector3(-100, 0, 0);
                        //newTrap.transform.localScale = tp.scale;
                    }

                    trapPool.Add(newTrap);
                }
            }
        }

    }

    public void ResetLevel()
    {
        StartMenuHelper.Instance.ResetStart();
        //groundPool[0].transform.position = groundStartPos;
        //groundPool[1].transform.position = new Vector3(-ground.transform.localScale.x, 0.0f, 0.0f);

        for (int i = 0; i < starPool.Count; i++)
        {
            starPool[i].transform.position = new Vector3((50 * (i + 1)), 0.0f, 0.0f);
        }

        nextPosition = 75.0f;
        if (currentTrapList.Count > 0)
        {
            foreach (GameObject go in currentTrapList)
            {
                trapPool.Add(go);
                go.SetActive(false);
            }
            currentTrapList.Clear();
        }

        PlaceTraps();
    }
}
